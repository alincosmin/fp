import Data.Char

data Parser a = Parser (String -> [(a, String)])

success :: a -> Parser a
success v = Parser (\inp -> [(v,inp)])

failure :: Parser a
failure = Parser (\inp -> [])

item :: Parser Char
item = Parser (
    \inp -> case inp of
        "" -> []
        (c:cs) -> [(c,cs)]
    )

parse :: Parser a -> String -> [(a, String)]
parse (Parser p) inp = p inp

instance Monad Parser where
    --return :: a -> Parser a
    return v = Parser (\inp -> [(v,inp)])
    
    --(>>=) :: Parser a -> (a -> Parser b) -> Parser b
    p >>= f = Parser (
        \inp -> case parse p inp of
            [] -> []
            [(v, out)] -> parse (f v) out
        )
p13 :: Parser (Char, Char)
p13 = item >>= \c1 -> item >>= \_ -> item >>= \c3 -> return (c1, c3)

p13' = do
    c1 <- item
    item
    c3 <- item
    return (c1, c3)
    
(+++) :: Parser a -> Parser a -> Parser a
p +++ q = Parser (
    \inp -> case parse p inp of
        [] -> parse q inp
        r -> r
    )

sat :: (Char -> Bool) -> Parser Char
sat p = do
    x <- item
    if p x then return x else failure
    
digit :: Parser Char
digit = sat isDigit

lower :: Parser Char
lower = sat isLower

upper :: Parser Char
upper = sat isUpper

letter :: Parser Char
letter = sat isAlpha

alphanum :: Parser Char
alphanum = sat isAlphaNum

char :: Char -> Parser Char
char x = sat (== x )

string :: String -> Parser String
string [] = return []
string s@(c:cs) = do
    char c
    string cs
    return s
    
many :: Parser a -> Parser [a]
many p = many1 p +++ return []

many1 :: Parser a -> Parser [a]
many1 p = do
    v <- p
    vs <- many p
    return (v:vs)
    
ident :: Parser String
ident = do
    c <- lower
    cs <- many alphanum
    return (c:cs)
    
nat :: Parser Int
nat = do
    n <- many1 digit
    return (read n::Int)
    
space :: Parser ()
space = do
    many (sat isSpace)
    return ()
    
token :: Parser a -> Parser a
token p = do
    space
    v <- p
    space
    return v
    
identifier :: Parser String
identifier = token ident

natural :: Parser Int
natural = token nat

symbol :: String -> Parser String
symbol s = token (string s)

emptyList :: Parser [a]
emptyList = do 
    symbol "["
    symbol "]"
    return []

intList1 :: Parser [Int]
intList1 = do
    symbol "["
    n <- natural
    ns <- many (do {symbol ","; natural})
    symbol "]"
    return (n:ns)

intList :: Parser [Int]
intList = emptyList +++ intList1