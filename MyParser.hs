import Data.Char

data Parser a = Parser (String -> [(a, String)])

success :: a -> Parser a
success a = Parser(\str -> [(a, str)])

failure :: Parser a
failure = Parser (\inp -> [])

item :: Parser Char
item = Parser (
    \inp -> case inp of
        "" -> []
        (c:cs) -> [(c,cs)]
    )

parse :: Parser a -> String -> [(a, String)]
parse (Parser p) inp = p inp


instance Monad Parser where
    --return :: a -> Parser a
    return v = Parser (\inp -> [(v,inp)])
    
    --(>>=) :: Parser a -> (a -> Parser b) -> Parser b
    p >>= f = Parser (
        \inp -> case parse p inp of
            [] -> []
            [(v, out)] -> parse (f v) out
        )

(+++) :: Parser a -> Parser a -> Parser a
p +++ q = Parser (
    \inp -> case parse p inp of
        [] -> parse q inp
        r -> r
    )

sat :: (Char -> Bool) -> Parser Char
sat p = do
    x <- item
    if p x then return x else failure
    
digit :: Parser Char
digit = sat isDigit

lower :: Parser Char
lower = sat isLower

upper :: Parser Char
upper = sat isUpper

letter :: Parser Char
letter = sat isAlpha

alphanum :: Parser Char
alphanum = sat isAlphaNum

char :: Char -> Parser Char
char x = sat (== x )

string :: String -> Parser String
string [] = return []
string s@(c:cs) = do
    char c
    string cs
    return s
    
many :: Parser a -> Parser [a]
many p = many1 p +++ return []

many1 :: Parser a -> Parser [a]
many1 p = do
    v <- p
    vs <- many p
    return (v:vs)
    
ident :: Parser String
ident = do
    c <- lower
    cs <- many alphanum
    return (c:cs)
    
nat :: Parser Float
nat = do
    n <- many1 digit
    return (read n::Float)
    
space :: Parser ()
space = do
    many (sat isSpace)
    return ()
    
token :: Parser a -> Parser a
token p = do
    space
    v <- p
    space
    return v
    
identifier :: Parser String
identifier = token ident

natural :: Parser Float
natural = token nat

symbol :: String -> Parser String
symbol s = token (string s)

expr :: Parser Float
expr =  term >>= \t ->
          (symbol "+" >>= \_ -> expr >>= \e -> return (t+e))
			+++ return t
			
term :: Parser Float
term = fnc >>= \f ->
		  (symbol "*" >>= \_ -> term >>= \t -> return (f*t))
			+++
		  (symbol "/" >>= \_ -> term >>= \t -> return (f/t))
			+++ return f

fnc :: Parser Float
fnc = (factor >>= \f -> (symbol "exp" >>= \_ -> factor >>= \t -> return (f**t)) +++ return f)
		+++ (symbol "sin" >>= \_ -> factor >>= \f -> return (sin f) +++ return f)
		+++ (symbol "cos" >>= \_ -> factor >>= \f -> return (cos f) +++ return f)
		+++	(symbol "log" >>= \_ -> factor >>= \f -> factor >>= \t -> return (logBase f t) +++ return f)
		
factor :: Parser Float
factor = (symbol "(" >>= \_ -> expr >>= \e -> symbol ")" >>= \_ -> return e)
		 +++ natural

eval :: String -> Float
eval xs = case parse expr xs of 
             [(n, [])] -> n
             [(_,out)] -> error ("unused input " ++ out)
             [] -> error "invalid input"
			 
			 
evalLine (x:xs) = do
					let res = eval x
					print res
					evalLine xs
								
evalLine [] = putStr ""

evalstart x = do 
	inputList <- readFile x
	let inputLine = lines inputList
	evalLine inputLine