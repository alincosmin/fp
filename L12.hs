import Data.Char

data Parser a = Parser (String -> [(a, String)])

success :: a -> Parser a
success a = Parser(\str -> [(a, str)])

failure :: Parser a
failure = Parser(\str -> [])

item :: Parser Char
item = Parser (function)
	where function str
			| length str > 0 = [(head str, tail str)]
			| otherwise = []
			
parse :: (Parser a) -> String -> [(a, String)]
parse (Parser x) s = x s

return :: a -> Parser a
return = success
(>>=) :: Parser a -> (a -> Parser b) -> Parser b