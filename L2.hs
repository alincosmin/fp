fact 1 = 1
fact n = n * fact (n-1)

sum' x y = x + y

prod' x y = x * y

acc f 1 = 1
acc f n = f n (acc f (n-1))

-- a+b - infixata
-- +ab - prefixata
-- ab+ - postfixata

-- List:
-- [1,2,3]
-- > 1:2:3:[]
-- [1,2,3]

-- List concatenation
-- > [1,2] ++ [3,4]

-- > [1,2,3] !! 1
-- 2

-- Range
-- > [1..5]
-- [1,2,3,4,5]

-- Range with different ratio
-- > [0,3..10
-- [0,3,6,9]

-- > head [1..10]
-- first element
-- > tail [1..10]
-- list without first element
-- > init [1..10]
-- first elements without the last
-- > last [1..10]
-- last element

-- other methods for lists: reverse, length
-- > elem 3 [1..3]
-- True

-- take/drop first elements
-- > take 3 [1..10]
-- > drop 3 [1..10] 

head' [] = error "lista goala"
head' (x:_) = x

second (_:x:_) = x

suml [] = 0
suml (x:xs) = x + suml xs

--insertSorted x xs = if head xs < x then insertSorted x (drop 1 xs)
--						else x : xs
insertSorted x [] = [x]
insertSorted x (y:xs) = if x <= y then x:y:xs else y:(insertSorted x xs)