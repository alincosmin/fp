sum_p (a,b) = a+b

-- :t something
-- gives type of object
 

ups [] = []
ups (c:cs) = if isUc c then c : ups(cs) else ups(cs) where isUc x = elem x ['A'..'Z']

-- :: Type
-- -> cast
-- Ex: pi :: Float

add' a b = a + b

mul x y = x * y

double x = (mul x) 2

double' = mul 2

double'' = (*2)

sqr = (^2)

pow2 = (2^)

-- mul3 x y z :: a->a->a->a
-- (((mul3 1) 2) 3) 

apply [] _ = []
apply (f:fs) x = f x : apply fs x