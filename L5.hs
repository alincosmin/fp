cz n = compare 10 n
cz' = compare 10
sqr = (^2)

app2 f x = f (f x)
-- map (^3) [1..10]
-- = (^3) on [1..10]

-- filter (<3) [1,2,3]
-- = [1,2]

-- takeWhile (<3) [1,2,3,0,5]
-- = [1,2]

-- zip [1,2,3] [0,1,2,3,4]
-- = [(1,0),(2,1),(3,2)]

-- zipWith (+) [1,2,3] [4,5,6]
-- = [5,7,9]

-- suma tuturor patratelor perfecte impare < x
mysum x = foldl (+) 0 (filter (odd) (map (^2) (takeWhile (<(truncate (sqrt x))) [1..])))

--add = \x y -> x+y

--add = \x -> \y -> x+y

-- map (\x -> 10*x+3) [1..10]
-- foldr (\x v -> v++[x]) [] [1..10]
-- = [10..1]

sum' xs = sum'' 0 xs
			where
				sum'' a [] = a
				sum'' a (x:xs) = sum'' (a+x) xs
				
-- foldl (\x v -> v:x) [] [1..10]
-- = [10..1]

-- fold (\a x -> x+a*2) 0 [1,1,0,1]
-- = 13

-- sum $ takeWhile (<100) $ filter odd $ map (^2) [1..]
-- = 165