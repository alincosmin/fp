nor :: Bool -> Bool -> Bool
nor a b = not (a || b)

nor' :: Bool -> Bool -> Bool
nor' False False = True
nor' _ _ = False

gcd' :: Integer -> Integer -> Integer
gcd' 0 a = a
gcd' a b = gcd b (mod a b)

gcd'' :: Integer -> Integer -> Integer
gcd'' a b 
			| a == b = a
			| a > b = gcd'' (a-b) b
			| a < b = gcd'' a (b-a)

num :: [Integer] -> Integer
num [] = 0
num xs = num (init xs) * 10 + mod (last xs) 10

interval a b xs = if a < b then xs !! a : interval (a+1) b xs else [xs !! b]

bin2int [] = 0
bin2int xs = bin2int (init xs) * 2 + last xs

int2bin 0 = []
int2bin x = int2bin (div x 2) ++ [mod x 2]

rot 0 xs = xs
rot 1 xs = [last xs] ++ init xs
rot (-1) xs = tail xs ++ [head xs]
rot x xs = if x > 0 then  else rot (x+1) xs
-- to be continued
-- ex: rot 3 [0..10] => [8..10,0..7]
-- ex: rot -3 [0..10] => [3..10,0,1,2]

